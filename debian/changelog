r-cran-haven (2.5.4-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 01 Dec 2023 06:37:40 -0600

r-cran-haven (2.5.3-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 06 Jul 2023 06:51:55 -0500

r-cran-haven (2.5.2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 12)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 06 Mar 2023 09:02:50 -0600

r-cran-haven (2.5.1-1) unstable; urgency=medium

  * New upstream release (to experimental during freeze)

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version  

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 26 Aug 2022 15:16:15 -0500

r-cran-haven (2.5.0-1) unstable; urgency=medium

  * New upstream release (to experimental during freeze)

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version  

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 15 Apr 2022 13:09:53 -0500

r-cran-haven (2.4.3-2) unstable; urgency=medium

  * Simple rebuild for unstable following Debian 11 release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 16 Aug 2021 14:13:04 -0500

r-cran-haven (2.4.3-1) experimental; urgency=medium

  * New upstream release (to experimental during freeze)

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 05 Aug 2021 22:53:09 -0500

r-cran-haven (2.4.1-1) experimental; urgency=medium

  * New upstream release (to experimental during freeze)

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version  
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 24 Apr 2021 16:23:37 -0500

r-cran-haven (2.3.1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 04 Jun 2020 20:27:35 -0500

r-cran-haven (2.3.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 28 May 2020 12:14:49 -0500

r-cran-haven (2.2.0-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 09 Nov 2019 17:12:43 -0600

r-cran-haven (2.1.1-2) unstable; urgency=medium

  * Source-only upload

  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 17 Aug 2019 09:33:43 -0500

r-cran-haven (2.1.1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 14 Jul 2019 11:24:26 -0500

r-cran-haven (2.1.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 23 Feb 2019 08:08:23 -0600

r-cran-haven (2.0.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 24 Nov 2018 18:49:46 -0600

r-cran-haven (1.1.2-1) unstable; urgency=medium

  * New upstream release

  * debian/patches/series: Comment out upstream_fix now in new release
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 01 Jul 2018 17:21:58 -0500

r-cran-haven (1.1.1-2) unstable; urgency=medium

  * Rebuilding for R 3.5.0 transition
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:

  * src/readstat/sas/readstat_sas7bcat_read.c: Upstream ReadStat patch 
  * src/readstat/spss/readstat_sav.c: Idem
  * src/readstat/spss/readstat_sav_read.c: Idem
  							(Closes: #899335)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 06 Jun 2018 21:11:10 -0500

r-cran-haven (1.1.1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 22 Jan 2018 20:08:22 -0600

r-cran-haven (1.1.0-1) unstable; urgency=low

  * Initial Debian release 				(Closes: #880939)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 05 Nov 2017 15:49:14 -0600
